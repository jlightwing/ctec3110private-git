<?php

    require 'vendor/autoload.php';

    $slimContainer = new Slim\Slim();

    require 'routes.php';

    $slimContainer->run();