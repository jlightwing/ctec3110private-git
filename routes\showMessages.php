<?php

    $slimContainer->get('/:messageCount', function($messageCount)
    {
        $ReceivedMessagesModel = new ReceivedMessagesModel();
        $messages = $ReceivedMessagesModel->getReceivedMessaged($messageCount);

        foreach ($messages as $message)
        {
            $parsedMessage = new SimpleXMLElement($message);
            echo(prettyPrintMessage($parsedMessage) . '<br />');
        }
    });

    function prettyPrintMessage($message)
    {
        $prettyMessage = "<b>Source MSISDN:</b>" . safeGetElement('sourcemsisdn', $message);
        $prettyMessage = $prettyMessage . "<b>Destination MSISDN: </b>" . safeGetElement('destinationmsisdn', $message);
        $prettyMessage = $prettyMessage . "<b>Received Time: </b>" . safeGetElement('receivedtime', $message);
        $prettyMessage = $prettyMessage . "<b>Bearer: </b>" . safeGetElement('bearer', $message);
        $prettyMessage = $prettyMessage . "<b>Message Ref: </b>" . safeGetElement('messageref', $message);
        $prettyMessage = $prettyMessage . "<b>Message: </b>" . safeGetElement('message', $message);

        return $prettyMessage;
    }

    function safeGetElement($index, $array)
    {
        if (array_key_exists($index, $array))
        {
            return $array->$index;
        }
    }