<?php

class ReceivedMessagesModel
{
    const SOAP_WASDL_URL = 'https://m2mconnect.ee.co.uk/orange-soap/services/MessageServiceByCountry?wsdl';
    const SOAP_USER = '15jlightwing';
    const SOAP_PASS = 'Password1';


    public function getReceivedMessaged($messageCount)
    {
        $soapClient = $this->createSoapClient();
        $receivedMessages = $this->peekMessages($messageCount, $soapClient);

        return $receivedMessages;
    }

    private function createSoapClient()
    {
        $client = new SoapClient(self::SOAP_WASDL_URL, array('trace' => 1, 'exceptions' => 0));
        return $client;
    }


    private function peekMessages($messageCount, SoapClient $soapClient)
    {
        $messages = $soapClient->peekMessages(self::SOAP_USER, self::SOAP_PASS, $messageCount, '');
        return $messages;
    }
}